;(function () {

function getTriggerForAuiPicker(element) {
    var cssClass, el;
    do {
        cssClass="adtp" + (new Date()).getTime();
        el = document.querySelector("." + cssClass);
    }
    while (el);
    element[0].className += " " + cssClass;
    return "." + cssClass;
}

angular.module("liferay7UITools", ["ngAnimate"])
    .factory("getLexiconIconUrl", getGetIconUrlFactory())
    .factory("getClayIconUrl", getGetIconUrlFactory(true))
    .directive("lexiconIcon", getIconDirectiveFactory())
    .directive("clayoidIcon", getIconDirectiveFactory(true)) // 'clayIcon' interferes with native clay-icon web element
    .directive("overloader", overloaderDirectiveFactory)
    .directive("simpleAlert", simpleAlertDirectiveFactory)
    .factory("showAlert", showAlertFactory)
    .factory("showError", showErrorFactory)
    .factory("showSuccess", showSuccessFactory)
    .directive("localizedInput", localizedInputDirectiveFactory)
    .directive("simpleLexValidate", simpleLexValidateDirectiveFactory)
    .directive("auiDatePicker", auiDatePickerDirectiveFactory)
    .directive("auiTimePicker", auiTimePickerDirectiveFactory)
    .directive("lexiconDialog", getDialogDirectiveFactory())
    .directive("clayDialog", getDialogDirectiveFactory(true))
    .factory("lexiconConfirm", getConfirmFactory())
    .factory("clayConfirm", getConfirmFactory(true));

function getGetIconUrlFactory(clay) {
    return function () {
        var liferayThemeImagesPath = Liferay.ThemeDisplay.getPathThemeImages();
        return function (iconId) {
            return liferayThemeImagesPath + "/" + (!clay ? "lexicon" : "clay") + "/icons.svg#" + iconId;
        }
    };
}

function getIconDirectiveFactory(clay) {
    var template = !clay ? "\
        <svg aria-hidden='true' class='icon-monospaced lexicon-icon'>\
            <use xlink:href='{{icon}}' />\
        </svg>\
    " : "\
        <svg class='lexicon-icon'>\
            <use xlink:href='{{icon}}' />\
        </svg>\
    "; // Для Clay в CSS-классе также слово "lexicon"
    iconDirectiveFactory.$inject = [!clay ? "getLexiconIconUrl" : "getClayIconUrl"];
    function iconDirectiveFactory(getIconUrl) {
        return {
            scope: {},
            restrict: "AE",
            replace: true,
            template: template,
            transclude: true,
            link: function (scope, element, attrs, ctrl /*not used*/, transclude) {
                scope.icon = getIconUrl(transclude(function (clone, scope) {}).text());
            }
        }
    }
    return iconDirectiveFactory;
}

overloaderDirectiveFactory.$inject = ["$compile"];
function overloaderDirectiveFactory($compile) {
    return {
        restrict: "A",
        scope: {
            overloader: "<"
        },
        link: function (scope, element, attrs) {
            var template = "\
                <div class='overloader' ng-show='show'>\
                    <div class='overloader-icon-container'>\
                        <div class='loading-animation'></div>\
                    </div>\
                </div>\
            ";
            var newScope = scope.$new(true);
            element.append($compile(template)(newScope));
            scope.$watch("overloader", function (newValue, oldValue) {
                newScope.show = newValue;
            });
        }
    };
}

function simpleAlertDirectiveFactory() {
    return {
        scope: {
            show: "=?",
            kind: "<",
            dismissible: "<",
            lead: "@",
            onDismiss: "<",
        },
        transclude: true,
        template: "\
            <div class='alert' role='alert' ng-class='\"alert-\"+kind+(dismissible?\" alert-dismissible\":\"\")' ng-if='show'>\
                <span class='alert-indicator' ng-if='kind==\"danger\"'>\
                    <clayoid-icon>exclamation-full</clayoid-icon>\
                </span>\
                <span class='alert-indicator' ng-if='kind==\"success\"'>\
                    <clayoid-icon>check-circle-full</clayoid-icon>\
                </span>\
                <span class='alert-indicator' ng-if='kind==\"info\"'>\
                    <clayoid-icon>info-circle</clayoid-icon>\
                </span>\
                <span class='alert-indicator' ng-if='kind==\"warning\"'>\
                    <clayoid-icon>warning-full</clayoid-icon>\
                </span>\
                <strong class='lead'>{{lead}}</strong>\
                <span ng-transclude></span>\
                <button class='close' type='button' ng-click='dismiss()' ng-if='dismissible'>\
                    <clayoid-icon>times</clayoid-icon>\
                </button>\
            </div>\
        ",
        link: function (scope, element, attrs) {
            scope.dismiss = function () {
                scope.show=false;
                if (angular.isFunction(scope.onDismiss)) {
                    scope.onDismiss();
                }
            };
        }
    };
}

showAlertFactory.$inject = ["$rootScope", "$rootElement", "$compile", "$timeout"];
function showAlertFactory($rootScope, $rootElement, $compile, $timeout) {
    var template = "<simple-alert class='fixed' show='true' kind='kind' dismissible='dismissible'\
            ng-if='show' on-dismiss='onDismiss' lead='{{lead}}'>{{message}}</simple-alert>";
    var scope = $rootScope.$new(true);
    scope.show = false;
    scope.onDismiss = function () {
        scope.show = false;
    };
    $rootElement.append($compile(template)(scope)); //элемент с алертом следует добавлять не в, например, body, а в $rootElement из-за особенностей работы ngAnimate
    var alertDismissionTask;
    var alertKinds = ["success", "info", "warning", "danger"];
    return function (lead, message, kind, dismissible) {
        if (alertDismissionTask) {
            $timeout.cancel(alertDismissionTask);
        }
        scope.lead = lead;
        scope.message = message;
        scope.kind = alertKinds.indexOf(kind) < 0 ? alertKinds[0] : kind;
        scope.dismissible = dismissible;
        scope.show = true;
        alertDismissionTask = $timeout(function () {
            scope.show = false;
            alertDismissionTask = undefined;
        }, 3000);
    };
}

showErrorFactory.$inject = ["showAlert"];
function showErrorFactory(showAlert) {
    return function (lead, message) {
        showAlert(lead, message, "danger", true);
    };
}

showSuccessFactory.$inject = ["showAlert"];
function showSuccessFactory(showAlert) {
    return function (lead, message) {
        showAlert(lead, message, "success", true);
    };
}

localizedInputDirectiveFactory.$inject = ["getLexiconIconUrl"];
function localizedInputDirectiveFactory(getLexiconIconUrl) {
    return {
        scope: {
            placeholder: "@",
            languages: "<",
            currentLang: "=?",
            onlyLangRequired: "<?",
        },

        require: 'ngModel',

        template: "\
            <span class='input-localized input-localized-textarea yui3-widget yui3-palette input-localized-focused' id='_com_liferay_journal_web_portlet_JournalPortlet_descriptionBoundingBox'>\
                <input class='form-control' placeholder='{{placeholder}}' type='text' ng-model='l7dValue[currentLang]' ng-if='!multiline'>\
                <textarea class='form-control' placeholder='{{placeholder}}' ng-model='l7dValue[currentLang]' ng-if='multiline'></textarea>\
                <div class='input-localized-content' role='menu'>\
                    <div class='palette-container'>\
                        <ul class='palette-items-container'>\
                            <li class='palette-item palette-item lfr-input-localized-default lfr-input-localized' data-index='0' role='menuitem' style='display: inline-block;' ng-class='{\"palette-item-selected\":lang==currentLang}' ng-repeat='lang in languages'>\
                                <a class='palette-item-inner' href='javascript:void(0)' tabindex='{{$index}}' ng-click='this.$parent.currentLang=lang'>\
                                    <span class='lfr-input-localized-flag'>\
                                        <svg class='lexicon-icon'>\
                                            <use xlink:href='{{getIconUrl(lang)}}'>\
                                        </svg>\
                                    </span>\
                                    <div class='lfr-input-localized-state'></div>\
                                </a>\
                            </li>\
                        </ul>\
                    </div>\
                </div>\
            </span>",

        link: function (scope, element, attrs, ngModel) {
            scope.getIconUrl = function (lang) {
                return getLexiconIconUrl(lang.toLowerCase().replace("_", "-"));
            };

            scope.currentLang = scope.languages[0];
            scope.multiline = attrs.multiline !== undefined;
            scope.l7dValue = {};

            ngModel.$render = function () {
                if (ngModel.$viewValue !== undefined) {
                    scope.l7dValue = ngModel.$viewValue;
                }
            };

            ngModel.$isEmpty = function (value) {
                if (value === undefined) return true;
                for (var lang in scope.languages) {
                    var langValue = value[scope.languages[lang]];
                    if (langValue && langValue.trim()) return false;
                }
                return true;
            };

            if (Object.keys(attrs).indexOf("onlyLangRequired") !== -1) {
                ngModel.$validators.onlyLangRequired = function (modelValue, viewValue) {
                    if (!scope.onlyLangRequired) return true;
                    if (!modelValue) return false;
                    var requiredLangValue = modelValue[scope.onlyLangRequired];
                    return requiredLangValue && requiredLangValue.trim();
                };
            }

            element.on("input" , function () {
                if (ngModel.$pristine) {
                    ngModel.$setDirty();
                }
                if (ngModel.$viewValue === undefined) {
                    ngModel.$setViewValue(scope.l7dValue);
                }
                else {
                    ngModel.$validate();
                }
                scope.$apply();
            });

            element.on("focusout" /*"blur" не подходит, потому что не всплывает*/ , function () {
                if (ngModel.$untouched) {
                    ngModel.$setTouched();
                    scope.$apply();
                }
            });
        }
    };
}

function simpleLexValidateDirectiveFactory() {
    function getErrorCssClasses(errorsHash) {
        return "has-error " + Object.keys(errorsHash).map(function (key) {
            return "error-" + key;
        }).join(" ");
    }

    function cleanContainer(container, validators) {
        var errorCssClasses = getErrorCssClasses(validators);
        container.removeClass("has-success");
        container.removeClass(errorCssClasses);
    }

    function setContainerStatusError(container, errorsHash) {
        var errorCssClasses = getErrorCssClasses(errorsHash);
        container.removeClass("has-success");
        container.addClass(errorCssClasses);
    }

    function setContainerStatusSuccess(container, errorsHash) {
        var errorCssClasses = getErrorCssClasses(errorsHash);
        container.addClass("has-success");
        container.removeClass(errorCssClasses);
    }

    function setContainerStatus(container, model) {
        if (model.$invalid) {
            setContainerStatusError(container, model.$error);
        }
        else {
            //в случае неошибки следует передавать $validators, так как в $error пусто
            setContainerStatusSuccess(container, model.$validators);
        }
    }

    return {
        restrict: 'A',

        require: 'ngModel',

        link: function (scope, element, attrs, ngModel) {
            var container = $(element).parents(".form-group");
            if (container.length) {
                scope.$watch(
                    function () {
                        return ngModel.$invalid;
                    }, function (newValue, oldValue) {
                        if (ngModel.$pristine) return;
                        setContainerStatus(container, ngModel);
                    }
                );
                scope.$watch(
                    function () {
                        return ngModel.$pristine;
                    }, function (newValue, oldValue) {
                        if (newValue) {
                            cleanContainer(container, ngModel.$validators);
                        }
                        else {
                            setContainerStatus(container, ngModel);
                        }
                    }
                );
            }
        }
    }
}

auiDatePickerDirectiveFactory.$inject = ["$rootScope"];
function auiDatePickerDirectiveFactory($rootScope) {
    return {
        template: "<input type='text' class='form-control' placeholder='{{placeholder}}' ng-readonly='readonly' ng-disabled='disabled'>",

        scope: {
            placeholder: "@"
        },

        restrict: "AE",

        require: 'ngModel',

        link: function (scope, element, attrs, ngModel) {
            var datePicker;
            var preInitStorage;
            var inputEl = element.find("input");

            YUI().use(
                "aui-datepicker",
                function (A) {
                    datePicker = new A.DatePicker({
                        trigger: getTriggerForAuiPicker(inputEl),
                        mask: '%d.%m.%Y',
                        popover: {
                            zIndex: 1051
                        },
                        on: {
                            selectionChange: function(event) {
                                var date = event.newSelection[0];
                                if (date && isNaN(+date)) {
                                    //Invalid Date
                                    //ситуация, возможная при некотором некорректном вводе строки, это вызывает
                                    //сбой в работе пикера, дальнейшие действия призваны восстановить его работу
                                    datePicker.clearSelection(true);
                                    setTimeout(function () {
                                        //версия AUI, применяемая в Liferay 7, для обхода проблемы здесь требует
                                        //вызова после задержки, поэтому setTimeout
                                        datePicker.selectDates(new Date());
                                        //к сожалению, сброс значения вместо выставления некоторой (в данном случае
                                        //текущей) не помогает избежать ошибки
                                    });
                                }
                                else {
                                    //при работе с версией AUI, применяемой в Liferay 6, желательна следующая строчка,
                                    //потому что в ней событие selectionChange срабатывает при фокусировке контрола и
                                    //дата при этом выбирается с текущим временем
                                    //date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12);
                                    ngModel.$setViewValue(date);
                                }
                            }
                        },
                    });
                    if (preInitStorage) {
                        inputEl.val(datePicker._formatDate(preInitStorage));
                        $rootScope.$apply();
                    }
                }
            );

            ngModel.$render = function () {
                if (!datePicker) {
                    preInitStorage = ngModel.$modelValue;
                }
                else {
                    inputEl.val(datePicker._formatDate(ngModel.$modelValue));
                }
            };

            inputEl.on("keydown", function (e) {
                if (e.key == "Enter") {
                    datePicker.hide();
                    inputEl[0].blur();
                }
            });

            inputEl.on("blur", function (e) {
                scope.$apply();
            });

            attrs.$observe("readonly", function(value) {
                scope.readonly = value;
            });

            attrs.$observe("disabled", function(value) {
                scope.disabled = value;
            });
        }
    };
}

auiTimePickerDirectiveFactory.$inject = ["$rootScope"];
function auiTimePickerDirectiveFactory($rootScope) {
    return {
        template: "<input type='text' class='form-control' ng-readonly='readonly' ng-disabled='disabled'>",

        scope: {},

        restrict: "AE",

        require: 'ngModel',

        link: function (scope, element, attrs, ngModel) {
            var timePicker;
            var preInitStorage;
            var inputEl = element.find("input");

            YUI().use(
                "aui-timepicker",
                function (A) {
                    timePicker = new A.TimePicker({
                        trigger: getTriggerForAuiPicker(inputEl),
                        mask: "%H:%M",
                        popover: {
                            zIndex: 1051
                        },
                        on: {
                            selectionChange: function(event) {
                                var date = event.newSelection[0];
                                if (date && isNaN(+date)) {
                                    //Invalid Date
                                    setTimeout(function () {
                                        //timePicker.clearSelection(); //этот вызов не работает (не очищает контрол)
                                        timePicker.selectDates([undefined]);
                                    });
                                }
                                else {
                                    ngModel.$setViewValue(date);
                                }
                            }
                        }
                    });
                    if (preInitStorage) {
                        inputEl.val(timePicker._formatDate(preInitStorage));
                        $rootScope.$apply();
                    }
                }
            );

            ngModel.$render = function () {
                if (!timePicker) {
                    preInitStorage = ngModel.$modelValue;
                }
                else {
                    inputEl.val(timePicker._formatDate(ngModel.$modelValue));
                }
            };

            inputEl.on("keydown", function (e) {
                if (e.key == "Enter") {
                    timePicker.hide();
                    inputEl[0].blur();
                }
            });

            inputEl.on("blur", function (e) {
                scope.$apply();
            });

            attrs.$observe("readonly", function(value) {
                scope.readonly = value;
            });

            attrs.$observe("disabled", function(value) {
                scope.disabled = value;
            });
        }
    };
}

function getDialogDirectiveFactory(clay) {
    var template = !clay ? "\
        <div class='fade modal lexicon-modal' role='dialog' tabindex='-1'>\
            <div class='modal-dialog' ng-class='dialogType'>\
                <div class='modal-content'>\
                    <div class='modal-header'>\
                        <button aria-labelledby='Close' class='btn btn-default close' data-dismiss='modal' role='button' type='button'>\
                            <lexicon-icon>times</lexicon-icon>\
                        </button>\
                        \
                        <button class='btn btn-default modal-primary-action-button visible-xs' type='button' ng-disabled='disabled' ng-click='doJob()'>\
                            <lexicon-icon>check</lexicon-icon>\
                        </button>\
                        \
                        <h4 class='modal-title' ng-transclude='heading'></h4>\
                    </div>\
                    \
                    <div class='modal-body' ng-transclude='body'></div>\
                    \
                    <div class='modal-footer' ng-transclude='footer' ng-show='footerSlotIsFilled'></div>\
                    <div class='modal-footer' ng-hide='footerSlotIsFilled'>\
                        <button class='btn btn-primary' type='button' ng-disabled='disabled' ng-click='doJob()' ng-transclude='primary-button'>OK</button>\
                        <button class='btn btn-link close-modal' data-dismiss='modal' type='button' ng-transclude='close-button'>Cancel</button>\
                    </div>\
                </div>\
            </div>\
        </div>\
    " : "\
        <div class='fade modal' role='dialog' tabindex='-1'>\
            <div class='modal-dialog' ng-class='dialogType'>\
                <div class='modal-content'>\
                    <div class='modal-header'>\
                        <div class='modal-title' ng-transclude='heading'></div>\
                        <button aria-labelledby='Close' class='close' data-dismiss='modal' role='button' type='button'>\
                            <clayoid-icon>times</clayoid-icon>\
                        </button>\
                    </div>\
                    <div class='modal-body' ng-transclude='body'></div>\
                    <div class='modal-footer'>\
                        <div class='modal-item-last'>\
                            <div class='btn-group' ng-transclude='footer' ng-show='footerSlotIsFilled'></div>\
                            <div class='btn-group' ng-hide='footerSlotIsFilled'>\
                                <div class='btn-group-item'>\
                                    <button class='btn btn-primary' type='button' ng-disabled='disabled' ng-click='doJob()' ng-transclude='primary-button'>OK</button>\
                                </div>\
                                <div class='btn-group-item'>\
                                    <button class='btn btn-secondary' data-dismiss='modal' type='button' ng-transclude='close-button'>Cancel</button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ";
    var selectorPrefix = !clay ? "lexicon" : "clay";
    return function () {
        return {
            template: template,

            transclude: {
                "heading": selectorPrefix + "DialogHeading",
                "body": selectorPrefix + "DialogBody",
                "footer": "?" + selectorPrefix + "DialogFooter",
                "primary-button": "?" + selectorPrefix + "DialogPrimaryButton",
                "close-button": "?" + selectorPrefix + "DialogCloseButton",
            },

            scope: {
                dialogType: "@type",
                cancellationToken: "=",
                disabled: "=",
                doJob: "&action",
                trigger: "=",
            },

            restrict: "AE",

            link: function (scope, element, attrs, ctrl /*not used*/, transclude) {
                var dialogElement = $(element).find(".modal");
                var isOpen = false;

                scope.footerSlotIsFilled = transclude.isSlotFilled("footer")

                dialogElement.on("hide.bs.modal", function (e) {
                    isOpen = false;
                    var cancellationToken = scope.cancellationToken;
                    if (cancellationToken) {
                        //cancellationToken предполагается использовать как способ, например, отменять HTTP-запрос, посланный
                        //"из интерфейса диалога", в этом случае отсутствие (значение null) токена в обработчике ошибки
                        //запроса следует трактовать как отмену запроса, а не собственно ошибку
                        scope.cancellationToken = null;
                        //разрешение токена следует делать после сброса значения, чтобы код имел возможность проверить
                        //отсутствие токена
                        if (scope.trigger) {
                            //закрытие вызвано "естественными" бутстрапными причинами (не по триггерному признаку), нужно
                            //провести digest-цикл для обновления значения токена во внешнем скопе
                            scope.$apply();
                            cancellationToken.resolve();
                        }
                        else {
                            //закрытие по триггерному признаку, в настоящее время уже выполняется digest-цикл, разрешение
                            //промиса следует отложить, иначе не будет возможности обнаружить отсутствие токена в коде
                            //обработчика ошибок
                            scope.$applyAsync(function () {
                                cancellationToken.resolve();
                            });
                        }
                    }
                    if (scope.trigger) {
                        scope.trigger = false;
                        scope.$apply();
                    }
                });

                scope.$watch("trigger", function (newValue, oldValue) {
                    if (newValue === oldValue) return;
                    if (newValue) {
                        //триггерный признак открытия взведён, следует показать диалог
                        dialogElement.modal();
                        isOpen = true;
                    }
                    else {
                        //триггерный признак открытия сброшен
                        if (isOpen) {
                            //событие "hide.bs.modal" ещё не срабатывало, следовательно, это начало закрытия диалога
                            //из ангулярного кода, нужно выполнить бутстрапный метод закрытия, чтобы скрыть диалог
                            dialogElement.modal("hide");
                        }
                        else {
                            //событие "hide.bs.modal" уже сработало, сброс триггерного признака был инициирован из
                            //обработчика этого события, диалог будет скрыт бутстрапным кодом, здесь ничего не нужно делать
                        }
                    }
                });
            }
        };
    };
}

function getConfirmFactory(clay) {
    var tagPrefix = !clay ? "lexicon" : "clay";
    var dialogTemplate = "\
        <" + tagPrefix + "-dialog type='{{dialogType}}' trigger='open' action='selectPrimary()' cancellation-token='cancellationToken'>\
            <" + tagPrefix + "-dialog-heading>${heading}</" + tagPrefix + "-dialog-heading>\
            <" + tagPrefix + "-dialog-body>${message}</" + tagPrefix + "-dialog-body>\
            ${primaryButton}\
            ${closeButton}\
        </" + tagPrefix + "-dialog>\
    ";
    var primaryButtonTemplate = "<" + tagPrefix + "-dialog-primary-button>${primaryButtonText}</" + tagPrefix + "-dialog-primary-button>";
    var closeButtonTemplate = "<" + tagPrefix + "-dialog-close-button>${closeButtonText}</" + tagPrefix + "-dialog-close-button>";
    confirmFactory.$inject = ["$rootScope", "$rootElement", "$compile", "$q"];
    function confirmFactory($rootScope, $rootElement, $compile, $q) {
        return function (message, heading, primaryButtonText, closeButtonText, dialogType) {
            if (!message) {
                throw new Error("'message' argument required, but not present");
            }
            if (!heading) {
                heading = "&nbsp;";
            }
            var template = dialogTemplate.replace("${heading}", heading);
            template = template.replace("${message}", message);
            if (primaryButtonText) {
                template = template.replace("${primaryButton}", primaryButtonTemplate.replace("${primaryButtonText}", primaryButtonText));
            }
            if (closeButtonText) {
                template = template.replace("${closeButton}", closeButtonTemplate.replace("${closeButtonText}", closeButtonText));
            }
            var scope = $rootScope.$new(true);
            scope.dialogType = dialogType;
            scope.open = false;
            var lexiconDialogElement = $compile(template)(scope);
            lexiconDialogElement.on("hidden.bs.modal", function (e) {
                lexiconDialogElement.remove();
                scope.$destroy();
            });
            $rootElement.append(lexiconDialogElement);
            scope.$applyAsync(function () {
                scope.open = true;
            });
            return $q(function(resolve, reject) {
                scope.selectPrimary = function () {
                    scope.open = false;
                    resolve(true);
                };
                scope.cancellationToken = {
                    resolve: function () {
                        resolve(false);
                    },
                };
            });
        };
    }
    return confirmFactory;
}

})();
